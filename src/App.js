import React, {PureComponent} from 'react';
import {connect} from 'react-redux';

import '../styles/default.scss';

class App extends PureComponent {

    render() {
        return 'Hello world';
    }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(App);
