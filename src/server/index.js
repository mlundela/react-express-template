import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import express from 'express';
import path from 'path';
import morgan from 'morgan';


const app = express();
app.use(cookieParser());
app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    'errors': {
      message: err.message,
      error: {},
    },
  });
});

async function start () {
  try {
    console.log('STARTING NODE SERVER');
    const port = process.env.PORT || 3000;
    app.listen(port, () => {
      console.log('NODE SERVER LISTENING ON PORT ' + port);
    });
  } catch (e) {
    setTimeout(start, 1000);
  }
}

setTimeout(start, 1000);
