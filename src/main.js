import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router';
import { HashRouter as Router } from 'react-router-dom';

import App from './App';

import { rootReducer } from './reducers';
import { store } from './store';
import Promise from 'bluebird';

const container = document.querySelector('#react-container') || (() => {
  const el = document.createElement('div');
  el.id = 'react-container';
  document.body.appendChild(el);
  return el;
})();

Promise.config({
  warnings: false,
});

render(
  <Provider store={store}>
    <Router path='/'>
      <Switch>
        <Route exact path='/' component={App}/>
        <Route render={() => <Redirect to='/'/>}/>
      </Switch>
    </Router>
  </Provider>,
  container,
);

if (module.hot) {
  module.hot.accept();
  module.hot.accept('./reducers', () => {
    store.replaceReducer(rootReducer);
  });
}
