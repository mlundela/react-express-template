const path = require('path');
const { HotModuleReplacementPlugin, NamedModulesPlugin } = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: ['babel-polyfill', './src/main.js'],
  output: {
    path: path.resolve(__dirname, 'dist/public'),
    publicPath: '/',
    filename: 'main.bundle.js',
  },
  module: {
    rules: [
      { test: /\.(js|jsx)$/, use: 'babel-loader', exclude: /node_modules/ },
      {
        test: /\.scss$/,
        loaders: [
          'style-loader',
          'css-loader?sourceMap',
          'sass-loader?sourceMap',
        ],
      },
      { test: /\.css$/, loader: 'style-loader!css-loader' },
      {
        test: /\.(jpe?g|gif|png|ico|svg)$/i,
        use: {
          loader: 'file-loader',
          options: { name: 'img/[name].[ext]?[hash]' },
        },
      },
    ],
  },
  devtool: 'source-map',
  devServer: {
    hot: true,
    inline: true,
    publicPath: '/',
    contentBase: './dist/public',
    disableHostCheck: true,
    proxy: {
      '/': 'http://localhost:3000/',
    },
  },
  plugins: [
    new HotModuleReplacementPlugin(),
    new NamedModulesPlugin(),
    new HtmlWebpackPlugin({
      template: 'template.html',
    }),
  ],
};
