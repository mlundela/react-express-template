const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
  target: 'node',
  node: {
    __dirname: false,
  },
  entry: [
    'babel-polyfill',
    path.resolve(__dirname, 'src/server/index.js'),
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
  externals: [
    nodeExternals({
      whitelist: [
        'babel-polyfill',
      ],
    }),
  ],
};
